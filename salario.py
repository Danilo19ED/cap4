#Mediante el ingreso del número de horas trabajadas
#y la tarifa por hora se obtendrá el salario del trabajador
#crear una función llamada calculo_salario que reciba dos
#parámetros (hora y tarifa)

#author : Danilo Delgado
#email: edwin.delgado@unl.edu.ec

hr = int(input("ingrese el número de horas trabajadas:  "))

tf = float(input("ingrese el valor por hora trabajada: "))


def calculo_salario(hora, tarifa):

    if hr > 40:
        hrt = hr - 40
        vex = (hrt * 1.5) * tf
        vb = (40 * tf) + vex
        print("el valor del salario es: ", vb)
    else:
        vb = hr * tf
        print("el valor del salario es: ", vb)


calculo_salario(hr, tf)
